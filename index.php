<!DOCTYPE html>
<html lang="en">

	<head>

	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="">
	    <meta name="author" content="">

	    <title>Leighton Store</title>

	    <!-- Bootstrap Core CSS -->
	    <link href="css/bootstrap.min.css" rel="stylesheet">

	    <!-- Custom CSS -->
	    <link href="css/main.css" rel="stylesheet">

	    <!-- Custom Fonts -->
	    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
	    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
	    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
	    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
	    <link href='https://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet' type='text/css'>

    </head>

    <body id="page-top" class="index">

        <!-- Header -->
        <section id="NavBarLeighton">
	        <header>
	            <div class="container">
	            	<div class="row">
	            		<div class ="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="NavBarMain">
	                		<ul class="list-inline intro-text text-center mt-2" id="NavList">
								<li class="list-inline-item"><a class="NavLinks" href="https://google.com"> work </a></li><li class="list-inline-item list-bullets">.</li>
								<li class="list-inline-item"><a class="NavLinks" href="https://google.com"> services </a></li><li class="list-inline-item list-bullets">.</li>
								<li class="list-inline-item"><a class="NavLinks" href="https://google.com"> insights </a></li><li class="list-inline-item list-bullets">.</li>
								<li class="list-inline-item"><a class="NavLinks" href="https://google.com"> blog </a></li><li class="list-inline-item list-bullets">.</li>
								<li class="list-inline-item"><a class="NavLinks" href="https://google.com"> events </a></li><li class="list-inline-item list-bullets">.</li>
								<li class="list-inline-item"><a class="NavLinks" href="https://google.com"> shop </a></li><li class="list-inline-item list-bullets">.</li>
								<li class="list-inline-item"><a class="NavLinks" href="https://google.com"> contact </a></li>
							</ul>
		            	</div>
	            	</div>
	            </div>
	        </header>
    	</section>

    	<section id="logo-section">
    		<div class="container">
    			<div class="row border-break">
    				<div class="col-lg-3 my-auto">
    					<div class="dropdown text-center">
							<button class="btn btn-secondary dropdown-toggle lang-button py-1 pr-0 px-1" type="button" id="dropdownMenuButtonLang" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    	<img class="img-fluid" src="img/gb.png" alt="">
							</button>
							<div class="dropdown-menu lang-items p-auto" aria-labelledby="dropdownMenuButton">
						    	<a class="dropdown-item" href="#">Action</a>
						    	<a class="dropdown-item" href="#">Another action</a>
						    	<a class="dropdown-item" href="#">Something else here</a>
						  	</div>
						</div>
    				</div>
    				<div class="col-lg-6 text-center py-3">
    					<img class="img-responsive" src="img/leighton-logo2.JPG" alt="">
    				</div>
    				<div class="col-lg-3">

    				</div>
    			</div>
    		</div>
    	</section>

    	<section id="shop-menu">
	            <div class="container">
	            	<div class="row border-break my-2">
	            		<div class ="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="MenuRow">
	                		<ul class="list-inline menu-text text-center mt-2" id="NavList">
								<li class="list-inline-item pr-2"><a class="menu-links" href="https://google.com"> Shop home </a></li><li class="list-inline-item menu-bullets pr-2">.</li>
								<li class="list-inline-item pr-3">
									<div class="dropdown">
										<button class="btn btn-secondary dropdown-toggle menu-button" type="button" id="dropdownMenuButtonMale" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									    Male
										</button>
										<div class="dropdown-menu menu-items" aria-labelledby="dropdownMenuButton">
									    	<a class="dropdown-item" href="#">Action</a>
									    	<a class="dropdown-item" href="#">Another action</a>
									    	<a class="dropdown-item" href="#">Something else here</a>
									  	</div>
									</div>
								</li>
								<li class="list-inline-item pr-3">
									<div class="dropdown">
										<button class="btn btn-secondary dropdown-toggle menu-button" type="button" id="dropdownMenuButtonFemale" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									    Female
										</button>
										<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									    	<a class="dropdown-item" href="#">Action</a>
									    	<a class="dropdown-item" href="#">Another action</a>
									    	<a class="dropdown-item" href="#">Something else here</a>
									  	</div>
									</div>
								</li>
								<li class="list-inline-item pr-3">
									<div class="dropdown">
										<button class="btn btn-secondary dropdown-toggle menu-button" type="button" id="dropdownMenuButtonKids" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									    Kids
										</button>
										<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									    	<a class="dropdown-item" href="#">Action</a>
									    	<a class="dropdown-item" href="#">Another action</a>
									    	<a class="dropdown-item" href="#">Something else here</a>
									  	</div>
									</div>
								</li>
								<li class="list-inline-item pr-2"><a class="menu-links" href="https://google.com"> Sale </a></li><li class="list-inline-item menu-bullets pr-2">.</li>
								<li class="list-inline-item"><a class="menu-links" href="https://google.com"> Coming soon </a></li>
							</ul>
		            	</div>
	            	</div>
	            </div>
    	</section>

    	<section id="search-sort">
	        <header>
	            <div class="container">
	            	<div class="row">
	            		<div class ="col-lg-1 py-2"> </div>
	            		<div class ="col-lg-5 py-2">
	            			<form class="form-inline">
								<i class="fas fa-search" aria-hidden="true"></i>
								<input class="form-control form-control-sm ml-3 w-75" type="text" placeholder="Search" aria-label="Search">
							</form>
	            		</div>
	            		<div class ="col-lg-3 py-2 offset-lg-2">
	            			<div class="row justify-content-end">
	            				<div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
	            					<div class="btn-group" role="group" aria-label="First group">
										<button class="btn love-button" type="button" id="LoveButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										    <i class="fas fa-heart" aria-hidden="true"></i>
										</button>
									</div>
									<div class="btn-group ml-2" role="group" aria-label="First group">
										<div class="dropdown">
											<button class="btn dropdown-toggle sort-display-button" type="button" id="dropdownMenuButtonSort" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										    	Sort by
											</button>
											<div class="dropdown-menu lang-items p-auto" aria-labelledby="dropdownMenuButton">
										    	<a class="dropdown-item" href="#">Action</a>
										    	<a class="dropdown-item" href="#">Another action</a>
										    	<a class="dropdown-item" href="#">Something else here</a>
										  	</div>	
										</div>
										<div class="dropdown ml-2">
											<button class="btn dropdown-toggle sort-display-button" type="button" id="dropdownMenuButtonDisplay" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										    	Display
											</button>
											<div class="dropdown-menu lang-items p-auto" aria-labelledby="dropdownMenuButton">
										    	<a class="dropdown-item" href="#">Action</a>
										    	<a class="dropdown-item" href="#">Another action</a>
										    	<a class="dropdown-item" href="#">Something else here</a>
										  	</div>
										</div>
									</div>
								</div>
							</div>
	            		</div>
	            		<div class ="col-lg-1 py-2"> </div>
	            	</div>
	            </div>
	        </header>
    	</section>

    	<section id="products-section">
	            <div class="container">
	            	<div class="row no-gutters-custom mb-1">
	            		<div class ="col-lg-1"> </div>
	            		<div class ="col-lg-10 border-break-full"> 
	            			<div class="row no-gutters-cutstom">
		            			<div class="col-lg-4">
		            				<img class="img-responsive float-left" src="img/test-product.JPG" alt="">
		            			</div>
		            			<div class="col-lg-8">
		            				<div class="row mt-2">
										<div class="col-lg-6">
											<h5 class="product-title">Brown Padded Jacket</h5>
										</div>
										<div class="col-lg-6">
											<div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
												<button class="btn love-button" type="button" id="LoveButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											    	<i class="fas fa-heart" aria-hidden="true"></i>
												</button>
												<h5 class="product-title mx-4 mt-1">&pound;65</h5>
											</div>
										</div>
		            				</div>
		            				<div class="row mt-1">
										<div class="col-lg-12">
											<h5 class="brand-title">Brand:</h5>
										</div>
		            				</div>
		            				<div class="row mt-1">
										<div class="col-lg-12">
											<h5 class="brand-title">Colour:</h5>
										</div>
		            				</div>
		            				<div class="row mt-1">
										<div class="col-lg-3">
											<div class="dropdown">
												<button class="btn dropdown-toggle sort-display-button" type="button" id="dropdownMenuButtonSort" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											    	Select size
												</button>
												<div class="dropdown-menu lang-items p-auto" aria-labelledby="dropdownMenuButton">
											    	<a class="dropdown-item" href="#">Action</a>
											    	<a class="dropdown-item" href="#">Another action</a>
											    	<a class="dropdown-item" href="#">Something else here</a>
											  	</div>	
											</div>
										</div>
										<div class="col-lg-9"> </div>
		            				</div>
		            				<div class="row mt-1">
										<div class="col-lg-12">
											<h5 class="brand-title">Description:</h5>
											<p class="text-muted description-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer hendrerit odio vitae lorem tempus, at imperdiet augue cursus. Proin aliquam, neque vel facilisis efficitur, metus nisi dapibus lacus, et blandit neque orci nec lorem. Praesent sodales congue quam, in consequat sem tincidunt nec. In pulvinar felis non blandit pellentesque. Aenean ac blandit urna. Nunc eget nunc at quam commodo cursus.</p>
										</div>
		            				</div>
		            				<div class="row mt-1">
										<div class="col-lg-12">
											<h5 class="brand-title">Product ID: <small class="text-muted description-text">000012234</small></h5>
										</div>
		            				</div>
		            			</div>
	            			</div>
	            		</div>
	            		<div class ="col-lg-1"> </div>
	            	</div>
	            </div>
    	</section>




		<footer>
			<header>
	            <div class="container">
	            	<div class="row">
	            		<div class ="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="FooterMain">
	                		<ul class="list-inline intro-text text-center mt-2" id="FooterList">
	                			<li>&copy;2019 the leighton shop</li>
							</ul>
		            	</div>
	            	</div>
	            </div>
	        </header>
		</footer>


<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

<!-- Contact Form JavaScript -->
<script src="js/jqBootstrapValidation.js"></script>

</body>

</html>
